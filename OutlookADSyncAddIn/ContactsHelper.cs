﻿using OutlookADAddIn.AD;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookADAddIn
{
    public class ContactsHelper
    {
        public static void MergeOutlookContact(Outlook.ContactItem olContact, ADContact adContact)
        {
            //Name
            olContact.FirstName = adContact.GivenName;
            olContact.LastName = adContact.SurName;
            olContact.MiddleName = adContact.Initials;

            //Picture
            SetPicture(olContact, adContact.Picture);

            //Company
            olContact.CompanyName = adContact.Company;
            olContact.JobTitle = adContact.Title;
            olContact.Department = adContact.Department;
            olContact.OfficeLocation = adContact.OfficeName;

            //E-mail
            olContact.Email1Address = adContact.Mail;
            //olContact.Email1DisplayName = adContact.DisplayName;

            //Phone numbers
            olContact.HomeTelephoneNumber = adContact.HomePhone;
            olContact.BusinessTelephoneNumber = adContact.TelephoneNumber;
            olContact.BusinessFaxNumber = adContact.FacsimileTelephoneNumber;
            olContact.PagerNumber = adContact.Pager;
            olContact.MobileTelephoneNumber = adContact.Mobile;

            //Address
            olContact.HomeAddressStreet = adContact.StreetAddress;
            olContact.HomeAddressCity = adContact.City;
            olContact.HomeAddressPostalCode = adContact.PostalCode;
            olContact.HomeAddressState = adContact.State;
            olContact.HomeAddressCountry = adContact.Country;

            //Notes
            olContact.Body = adContact.Info;
        }


        public static void SetPicture(Outlook.ContactItem olContact, byte[] picture)
        {
            if (picture == null)
            {
                olContact.RemovePicture();
                olContact.User1 = null;
            }
            else
            {
                var tmpFile = Path.GetTempFileName();
                File.WriteAllBytes(tmpFile, picture);
                olContact.AddPicture(tmpFile);
                File.Delete(tmpFile);
                var hash = GetHash(picture);
                olContact.User1 = hash;
            }
        }

        /// <summary>
        /// Calculate an MD5 hash string from an array of bytes.
        /// </summary>
        /// <param name="data">Data to hash.</param>
        /// <returns>Hash array.</returns>
        private static string GetHash(byte[] data)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var hash = md5.ComputeHash(data);
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sBuilder.Append(hash[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }



        public static string CompareOutlookAdContacts(Outlook.ContactItem olContact, ADContact adContact)
        {
            string changedFields = "";

            //Name
            if (!ContactsHelper.AreStringsSame(olContact.FirstName, adContact.GivenName)) { changedFields += "FirstName;"; }
            if (!ContactsHelper.AreStringsSame(olContact.LastName, adContact.SurName)) { changedFields += "LastName;"; }
            if (!ContactsHelper.AreStringsSame(olContact.MiddleName, adContact.Initials)) { changedFields += "MiddleName;"; }
            
            //Picture
            var adPicHash = adContact.Picture == null ? null : GetHash(adContact.Picture);
            if (adPicHash != olContact.User1) { changedFields += "Picture;"; };

            //Company
            if (!ContactsHelper.AreStringsSame(olContact.CompanyName, adContact.Company)) { changedFields += "CompanyName;"; }
            if (!ContactsHelper.AreStringsSame(olContact.JobTitle, adContact.Title)) { changedFields += "JobTitle;"; }
            if (!ContactsHelper.AreStringsSame(olContact.Department, adContact.Department)) { changedFields += "Department;"; }
            if (!ContactsHelper.AreStringsSame(olContact.OfficeLocation, adContact.OfficeName)) { changedFields += "Office;"; }

            //E-mail
            if (!ContactsHelper.AreStringsSame(olContact.Email1Address, adContact.Mail)) { changedFields += "E-mail;"; }
            //if (!ContactsHelper.AreStringsSame(olContact.Email1DisplayName, adContact.DisplayName)) { changedFields += "EmailDisplayName;"; }

            //Phone numbers
            //NOTE: Spaces (' ') are ignored when comparing phone numbers!
            if (!ContactsHelper.AreStringsSame(olContact.HomeTelephoneNumber, adContact.HomePhone, true)) { changedFields += "HomePhone;"; }
            if (!ContactsHelper.AreStringsSame(olContact.BusinessTelephoneNumber, adContact.TelephoneNumber, true)) { changedFields += "BusinessPhone;"; }
            if (!ContactsHelper.AreStringsSame(olContact.BusinessFaxNumber, adContact.FacsimileTelephoneNumber, true)) { changedFields += "BusinessFax;"; }
            if (!ContactsHelper.AreStringsSame(olContact.PagerNumber, adContact.Pager, true)) { changedFields += "Pager;"; }
            if (!ContactsHelper.AreStringsSame(olContact.MobileTelephoneNumber, adContact.Mobile, true)) { changedFields += "MobilePhone;"; }

            //Address
            if (!ContactsHelper.AreStringsSame(olContact.HomeAddressStreet, adContact.StreetAddress)) { changedFields += "StreetAddress;"; }
            if (!ContactsHelper.AreStringsSame(olContact.HomeAddressCity, adContact.City)) { changedFields += "City;"; }
            if (!ContactsHelper.AreStringsSame(olContact.HomeAddressPostalCode, adContact.PostalCode)) { changedFields += "PostalCode;"; }
            if (!ContactsHelper.AreStringsSame(olContact.HomeAddressState, adContact.State)) { changedFields += "State;"; }
            if (!ContactsHelper.AreStringsSame(olContact.HomeAddressCountry, adContact.Country)) { changedFields += "Country;"; }

            //Notes
            if (!ContactsHelper.AreStringsSame(olContact.Body, adContact.Info)) { changedFields += "Notes;"; }

            return changedFields;
        }

        public static List<ContactListViewItem> FindNewAndUpdatedAdContacts(ADContact[] adContacts, Outlook.Items olContacts)
        {
            List<ContactListViewItem> items = new List<ContactListViewItem>();

            //Add all AD contacts to list view
            foreach (ADContact adContact in adContacts)
            {
                //if (shutdown)
                //    return null;

                ContactListViewItem contactItem = new ContactListViewItem();
                contactItem.AdName = adContact.DisplayName;

                Outlook.ContactItem olContact = OutlookInterface.GetOutlookContact(olContacts, adContact.DisplayName, adContact.Company);
                if (olContact != null)
                {
                    contactItem.OlName = olContact.FullName;

                    string change = ContactsHelper.CompareOutlookAdContacts(olContact, adContact);
                    if (string.IsNullOrEmpty(change))
                    {
                        contactItem.Action = ContactAction.None;
                        contactItem.Change = "";
                    }
                    else
                    {
                        contactItem.Action = ContactAction.Update;
                        contactItem.Change = change;
                    }
                }
                else
                {
                    contactItem.OlName = "-";
                    contactItem.Action = ContactAction.Add;
                    contactItem.Change = "(*)";
                }

                items.Add(contactItem);
            }

            return items;
        }

        public static void FindOutlookContactsDeletedInAD(Outlook.Items olContacts, string companyName,  List<ContactListViewItem> items)
        {
            Outlook.ContactItem olContact = olContacts.Find(string.Format("[CompanyName] = \"{0}\"", companyName)) as Outlook.ContactItem;
            while (olContact != null)
            {
                //if (shutdown)
                //    return;

                bool exists = items.Exists(delegate(ContactListViewItem contact) { return contact.AdName.Equals(olContact.FullName); });
                if (exists)
                {
                    //The contact has alread been processed in the previous loop, since the same name exists in both AD and Outlook!
                }
                else
                {
                    //The contact exists in Outlook but not in the AD; delete
                    ContactListViewItem contactItem = new ContactListViewItem();
                    contactItem.AdName = "-";
                    contactItem.OlName = olContact.FullName;
                    contactItem.Action = ContactAction.Delete;
                    contactItem.Change = "";

                    items.Add(contactItem);
                }

                olContact = olContacts.FindNext() as Outlook.ContactItem;
            }
        }

        public static bool HasContactsChanged(Outlook.Items olContacts, string companyName)
        {
            ADContact[] adContacts = ADInterface.GetAdContacts(companyName);

            List<ContactListViewItem> items = ContactsHelper.FindNewAndUpdatedAdContacts(adContacts, olContacts);
            ContactsHelper.FindOutlookContactsDeletedInAD(olContacts, companyName, items);

            foreach (ContactListViewItem contact in items)
            {
                if (!string.IsNullOrEmpty(contact.Change))
                    return true;
            }

            return false;
        }

        private static bool AreStringsSame(string strA, string strB)
        {
            return AreStringsSame(strA, strB, false);
        }

        private static bool AreStringsSame(string strA, string strB, bool isPhoneNumber)
        {
            string a = strA == null ? "" : strA.Trim();
            string b = strB == null ? "" : strB.Trim();

            if (isPhoneNumber)
            {
                a = a.Replace(" ", "").Replace("(", "").Replace(")", "");
                b = b.Replace(" ", "").Replace("(", "").Replace(")", "");
            }

            return (a.CompareTo(b) == 0);
        }
    }
}
