﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using OutlookADAddIn.Helpers;

namespace OutlookADAddIn
{
    public partial class OutlookADAddIn
    {
        private void OutlookADAddIn_Startup(object sender, System.EventArgs e)
        {
            //if (Properties.Settings.Default.LoggingEnabled)
            //{
            //    Logger.Initialize();
            //}

            //Logger.WriteLine("OutlookADAddIn.OutlookADAddIn_Startup()");

            //if (Properties.Settings.Default.CheckForChangesOnStartup)
            //{
            //    ContactManager contactManager = new ContactManager(this.Application);
            //    contactManager.CheckForChanges();
            //}
        }

        private void OutlookADAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon();
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(OutlookADAddIn_Startup);
            this.Shutdown += new System.EventHandler(OutlookADAddIn_Shutdown);
        }
        
        #endregion
    }
}
