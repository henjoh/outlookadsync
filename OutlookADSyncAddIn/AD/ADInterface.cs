﻿using OutlookADAddIn.Helpers;
using System.Collections.Generic;
using System.DirectoryServices;

namespace OutlookADAddIn.AD
{
    internal class ADInterface
    {
        /// <summary>
        /// Get AD contacts matching companyName.
        /// </summary>
        /// <returns></returns>
        public static ADContact[] GetAdContacts(string companyName)
        {
            string filter = string.Format("(&(objectCategory=person)(objectClass=user)(company={0}))", companyName);
            return ADInterface.SearchAd(filter);
        }

        public static ADContact GetAdContact(string companyName, string displayName)
        {
            string filter = string.Format("(&(objectCategory=person)(objectClass=user)(company={0})(displayName={1}))", companyName, displayName);
            ADContact[] result = ADInterface.SearchAd(filter);
            if (result.Length > 0)
            {
                return result[0];
            }
            else
            {
                return null;
            }
        }

        private static ADContact[] SearchAd(string filter)
        {
            DirectoryEntry rootDSE = new DirectoryEntry("LDAP://rootDSE");
            string domainDN = rootDSE.Properties["DefaultNamingContext"].Value.ToString();
            DirectoryEntry adEntry = new DirectoryEntry("LDAP://" + domainDN);

            Logger.WriteLine(string.Format("Searching AD at '{0}' for entries matching '{1}'.", adEntry.Path, filter));

            DirectorySearcher searcher = new DirectorySearcher(adEntry);
            searcher.Filter = filter;
            searcher.SearchScope = SearchScope.Subtree;

            List<ADContact> result = new List<ADContact>();

            SearchResultCollection searchResultCollection = searcher.FindAll();
            foreach (SearchResult sr in searchResultCollection)
            {
                result.Add(new ADContact(sr.GetDirectoryEntry()));
            }

            return result.ToArray();
        }
    }
}
