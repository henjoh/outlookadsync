﻿using System.DirectoryServices;

namespace OutlookADAddIn.AD
{
    public class ADContact
    {
        private DirectoryEntry adContact = null;

        public ADContact(DirectoryEntry adContact)
        {
            this.adContact = adContact;
        }

        public string GivenName
        {
            get { return adContact.Properties["givenname"].Value.ToString(); }
        }

        public string SurName
        {
            get { return adContact.Properties["sn"].Value.ToString(); }
        }

        //public string FullName
        //{
        //    get { return adContact.Properties["FullName"].Value != null ? adContact.Properties["FullName"].Value.ToString() : ""; }
        //}

        public string Initials
        {
            get { return adContact.Properties["Initials"].Value != null ? adContact.Properties["Initials"].Value.ToString() : ""; }
        }

        public string Company
        {
            get { return adContact.Properties["company"].Value.ToString(); }
        }

        public string Title
        {
            get { return adContact.Properties["Title"].Value != null ? adContact.Properties["Title"].Value.ToString() : ""; }
        }

        public string Department
        {
            get { return adContact.Properties["Department"].Value != null ? adContact.Properties["Department"].Value.ToString() : ""; }
        }

        public string OfficeName
        {
            get { return adContact.Properties["physicalDeliveryOfficeName"].Value != null ? adContact.Properties["physicalDeliveryOfficeName"].Value.ToString() : ""; }
        }

        public string Mail
        {
            get { return (adContact.Properties["mail"].Value ?? "").ToString(); }
        }

        public string DisplayName
        {
            get { return adContact.Properties["displayName"].Value.ToString(); }
        }

        public string HomePhone
        {
            get { return adContact.Properties["homePhone"].Value != null ? adContact.Properties["homePhone"].Value.ToString() : ""; }
        }

        public string TelephoneNumber
        {
            get { return adContact.Properties["telephoneNumber"].Value != null ? adContact.Properties["telephoneNumber"].Value.ToString() : ""; }
        }

        public string FacsimileTelephoneNumber
        {
            get { return adContact.Properties["facsimileTelephoneNumber"].Value != null ? adContact.Properties["facsimileTelephoneNumber"].Value.ToString() : ""; }
        }

        public string Pager
        {
            get { return adContact.Properties["pager"].Value != null ? adContact.Properties["pager"].Value.ToString() : ""; }
        }

        public string Mobile
        {
            get { return adContact.Properties["mobile"].Value != null ? adContact.Properties["mobile"].Value.ToString() : ""; }
        }

        public string StreetAddress
        {
            get { return adContact.Properties["StreetAddress"].Value != null ? adContact.Properties["StreetAddress"].Value.ToString() : ""; }
        }

        public string City
        {
            get { return adContact.Properties["l"].Value != null ? adContact.Properties["l"].Value.ToString() : ""; }
        }

        public string PostalCode
        {
            get { return adContact.Properties["PostalCode"].Value != null ? adContact.Properties["PostalCode"].Value.ToString() : ""; }
        }

        public string State
        {
            get { return adContact.Properties["st"].Value != null ? adContact.Properties["st"].Value.ToString() : ""; }
        }

        public string Country
        {
            get { return adContact.Properties["co"].Value != null ? adContact.Properties["co"].Value.ToString() : ""; }
        }

        public string Info
        {
            get { return adContact.Properties["info"].Value != null ? adContact.Properties["info"].Value.ToString() : ""; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public byte[] Picture
        {
            get { return (byte[])adContact.Properties["thumbnailPhoto"].Value; }
        }
    }
}
