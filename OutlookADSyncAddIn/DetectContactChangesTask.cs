﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using OutlookADAddIn.Helpers;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookADAddIn
{
    class DetectContactChangesTask
    {
        private Outlook.Application outlookApplication;
        private Thread checkForChangeThread;
        private event EventHandler ChangesFoundEvent;
        
        public DetectContactChangesTask(Outlook.Application application)
        {
            outlookApplication = application;            
        }

        public void CheckForChanges()
        {
            ChangesFoundEvent += new EventHandler(EmployeeAddIn_ChangesFoundEvent);
            checkForChangeThread = new Thread(new ThreadStart(CheckForChangeCore));
            checkForChangeThread.SetApartmentState(ApartmentState.STA);
            checkForChangeThread.Start();
        }

        void EmployeeAddIn_ChangesFoundEvent(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Contact list has changed in AD! Do you want to run an update now?", StringsRes.Lang.MessageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                UpdateContactsDialog dlg = new UpdateContactsDialog();
                dlg.ShowDialog();
            }
        }

        private void CheckForChangeCore()
        {
            try
            {
                Outlook.MAPIFolder contactsFolder = OutlookInterface.GetSelectedContactsFolder();
                bool result = ContactsHelper.HasContactsChanged(contactsFolder.Items, Properties.Settings.Default.CompanyName);
                if (result)
                {
                    ChangesFoundEvent(this, new EventArgs());
                }
                Marshal.ReleaseComObject(contactsFolder);
            }
            catch (COMException e)
            {
                Logger.WriteException(e);
                if (e.Message.Contains("The specified domain either does not exist"))
                {
                    // This probably means we don't have any network connection. Ignore it here while we're starting up. We don't want loads of popups.
                }
                else
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
                MessageBox.Show("Failed to check Outlook contacts. Error message: " + e.Message,
                    StringsRes.Lang.MessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
