﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using OutlookADAddIn.Helpers;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;

// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new Ribbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace OutlookADAddIn
{
    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;

        public Ribbon()
        {
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            string ribbonXML = string.Empty;

            if (ribbonID.Equals("Microsoft.Outlook.Explorer"))
            {
                ribbonXML = GetResourceText("OutlookADAddIn.Ribbon.xml");
            }

            return ribbonXML;
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, select the Ribbon XML item in Solution Explorer and then press F1

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;

            Logger.WriteLine("Ribbon.Ribbon_Load()");
        }

        public void updateContacts_Click(Office.IRibbonControl control)
        {
            UpdateContactsDialog dlg = new UpdateContactsDialog();
            dlg.ShowDialog();
        }

        public void syncSettings_Click(Office.IRibbonControl control)
        {
            SyncSettingsDialog dlg = new SyncSettingsDialog();
            dlg.ShowDialog();
        }

        public void aboutButton_Click(Office.IRibbonControl control)
        {
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                MessageBox.Show(
                    string.Format("Version: {0}", System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString(4)),
                    "About Outlook AD Sync Add-in");
            }
            else
            {
                MessageBox.Show("Version: Internal Test", "About Outlook AD Sync Add-in");
            }
        }

        public void showFullPath_Click(Office.IRibbonControl control)
        {
            Outlook.Application application = Globals.OutlookADAddIn.Application;
            
            foreach (Object obj in application.ActiveExplorer().Selection)
            {
                if (obj is Outlook.MailItem)
                {
                    Outlook.MailItem mailItem = obj as Outlook.MailItem;
                    MessageBox.Show(string.Format("Full folder path of this item is:\n{0}", ((Outlook.Folder)mailItem.Parent).FolderPath), 
                        "Item Full Path", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
