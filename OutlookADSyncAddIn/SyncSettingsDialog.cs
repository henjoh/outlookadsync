﻿using System;
using System.Windows.Forms;
using OutlookADAddIn.Helpers;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookADAddIn
{
    public partial class SyncSettingsDialog : Form
    {
        public SyncSettingsDialog()
        {
            InitializeComponent();
        }
        
        private void SyncSettingsDialog_Load(object sender, EventArgs e)
        {
            companyNameTextBox.Text = Properties.Settings.Default.CompanyName;

            //Get list of available contact folders
            Outlook.MAPIFolder defaultOutlookContactsFolder = OutlookInterface.GetDefaultContactsFolder();
            contactFoldersComboBox.Items.Clear();
            contactFoldersComboBox.SelectedIndex = contactFoldersComboBox.Items.Add(defaultOutlookContactsFolder);
            foreach (Outlook.MAPIFolder f in defaultOutlookContactsFolder.Folders)
            {
                int i = contactFoldersComboBox.Items.Add(f);
                if (f.Name == Properties.Settings.Default.OutlookContactsFolder)
                {
                    contactFoldersComboBox.SelectedIndex = i;
                }
            }

            this.RefreshUI();
        }

        private void RefreshUI()
        {
            checkForChangesCheckBox.Checked = Properties.Settings.Default.CheckForChangesOnStartup;
            enableLoggingCheckBox.Checked = Properties.Settings.Default.LoggingEnabled;
        }

        private void contactFoldersComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Properties.Settings.Default.OutlookContactsFolder = (contactFoldersComboBox.SelectedItem as Outlook.MAPIFolder).Name;
            Properties.Settings.Default.Save();
        }

        private void checkForChangesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.CheckForChangesOnStartup = checkForChangesCheckBox.Checked;
            Properties.Settings.Default.Save();
        }

        private void enableLoggingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            Logger.Initialized = enableLoggingCheckBox.Checked;

            Properties.Settings.Default.LoggingEnabled = enableLoggingCheckBox.Checked;
            Properties.Settings.Default.Save();
        }

        private void companyNameTextBox_Leave(object sender, EventArgs e)
        {
            Properties.Settings.Default.CompanyName = companyNameTextBox.Text;
            Properties.Settings.Default.Save();
        }
    }
}
