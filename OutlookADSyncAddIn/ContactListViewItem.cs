﻿using System.Windows.Forms;

namespace OutlookADAddIn
{
    public enum ContactAction
    {
        None = 0,
        Update,
        Add,
        Delete
    }

    public class ContactListViewItem
    {
        private string adName;
        private string olName;
        private ContactAction action;
        //private string actionText;
        private string change;

        public string AdName
        {
            get { return adName; }
            set { adName = value; }
        }

        public string  OlName 
        {
            get { return olName; }
            set { olName = value; }
        }

        public ContactAction Action
        {
            get { return action; }
            set { action = value; }
        }

        public string Change
        {
            get { return change; }
            set { change = value; }
        }

        public ListViewItem ToListViewItem()
        {
            ListViewItem lvi = new ListViewItem(adName);
            lvi.SubItems.Add(olName);
            lvi.SubItems.Add(this.GetActionText());
            lvi.SubItems.Add(change);
            lvi.Tag = action;
            lvi.Checked = (action != ContactAction.None);
            if (!lvi.Checked)
            {
                lvi.ForeColor = System.Drawing.Color.DarkGray;
            }
            return lvi;
        }

        private string GetActionText()
        {
            string actionText;

            switch (action)
            {
                case ContactAction.Update:
                    actionText = StringsRes.Lang.ActionUpdate;
                    break;
                case ContactAction.Add:
                    actionText = StringsRes.Lang.ActionAdd;
                    break;
                case ContactAction.Delete:
                    actionText = StringsRes.Lang.ActionDelete;
                    break;
                default:
                    actionText = "-";
                    break;
            }

            return actionText;
        }
    }
}
