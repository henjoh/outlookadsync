﻿using OutlookADAddIn.AD;
using OutlookADAddIn.Helpers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

//TODO:
//- Trådning
//- Felhantering
//- Localization

namespace OutlookADAddIn
{
    public partial class UpdateContactsDialog : Form
    {
        private Outlook.MAPIFolder selectedOutlookContactsFolder;
        private Thread populateContactListViewThread = null;
        private ListViewColumnSorter listViewColumnSorter;
        private bool shutdown = false;
        private bool isUpdating = false;

        private List<ContactListViewItem> contactListViewItems = new List<ContactListViewItem>();

        public UpdateContactsDialog()
        {
            InitializeComponent();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            if (!isUpdating)
            {
                isUpdating = true;
                runButton.Enabled = closeButton.Enabled = false;
                
                this.UpdateOutlookContacts();
                this.RefreshListView();

                isUpdating = false;
            }
        }

        private void UpdateContactsDialog_Load(object sender, EventArgs e)
        {
            listViewColumnSorter = new ListViewColumnSorter();
            contactListView.CheckBoxes = true;
            contactListView.ListViewItemSorter = listViewColumnSorter;

            selectedOutlookContactsFolder = OutlookInterface.GetSelectedContactsFolder();

            outlookContactsFolderLabel.Text = selectedOutlookContactsFolder.FolderPath;

            //Refresh list view
            this.RefreshListView();
        }

        private void RefreshListView()
        {
            populateContactListViewThread = new Thread(new ThreadStart(PopulateContactListView));
            populateContactListViewThread.Start();
        }

        private void RefreshGui(bool enable)
        {
            //contactFoldersComboBox.Enabled = enable;
            refreshButton.Enabled = enable;
            runButton.Enabled = enable ? contactListView.Items.Count > 0 : false;
            closeButton.Enabled = enable;

            if (enable)
            {
                //Adjust contact list view column widths to content
                contactListView.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.ColumnContent);
                contactListView.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.ColumnContent);
                contactListView.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.HeaderSize);
                contactListView.AutoResizeColumn(3, ColumnHeaderAutoResizeStyle.HeaderSize);
            }

            listViewColumnSorter.SortColumn = 2;
            listViewColumnSorter.SortOrder = SortOrder.Descending;
            this.contactListView.Sort();
        }

        private void UpdateOutlookContacts()
        {
            toolStripProgressBar.Minimum = 0;
            toolStripProgressBar.Maximum = contactListView.CheckedItems.Count;
            toolStripProgressBar.Step = 1;

            foreach (ListViewItem li in contactListView.CheckedItems)
            {
                Outlook.ContactItem olContact = null;

                ContactAction action = (ContactAction)li.Tag;
                
                switch (action)
                {
                    case ContactAction.Update:
                        {
                            this.SetToolStripStatusTextCore(string.Format(StringsRes.Lang.UpdatingContact, li.Text));
                            ADContact adContact = ADInterface.GetAdContact(Properties.Settings.Default.CompanyName, li.Text);
                            olContact = selectedOutlookContactsFolder.Items.Find(string.Format("[CompanyName] = '{0}' And [FullName] = \"{1}\"", 
                                Properties.Settings.Default.CompanyName, adContact.DisplayName)) as Outlook.ContactItem;
                            ContactsHelper.MergeOutlookContact(olContact, adContact);
                            olContact.Save();
                        }
                        break;

                    case ContactAction.Add:
                        {
                            this.SetToolStripStatusTextCore(string.Format(StringsRes.Lang.AddingContact, li.Text));
                            ADContact adContact = ADInterface.GetAdContact(Properties.Settings.Default.CompanyName, li.Text);
                            olContact = selectedOutlookContactsFolder.Items.Add(Outlook.OlItemType.olContactItem) as Outlook.ContactItem;
                            ContactsHelper.MergeOutlookContact(olContact, adContact);
                            olContact.Save();
                        }
                        break;

                    case ContactAction.Delete:
                        {
                            this.SetToolStripStatusTextCore(string.Format(StringsRes.Lang.DeletingContact, li.SubItems[1].Text));
                            olContact = selectedOutlookContactsFolder.Items.Find(string.Format("[CompanyName] = '{0}' And [FullName] = \"{1}\"",
                                Properties.Settings.Default.CompanyName, li.SubItems[1].Text)) as Outlook.ContactItem;
                            olContact.Delete();
                        }
                        break;
                    
                    case ContactAction.None:
                        //Do nothing
                        break;
                    
                    default:
                        MessageBox.Show(string.Format(StringsRes.Lang.UnknownAction, action), StringsRes.Lang.ErrorDialogCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                if (olContact != null)
                {
                    Marshal.ReleaseComObject(olContact);
                }

                toolStripProgressBar.PerformStep();
            }

            this.SetToolStripStatusTextCore(StringsRes.Lang.Done);
        }

        #region Methods for thread safe access to GUI elements

        private void SetToolStripStatusText(string statusText)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { this.SetToolStripStatusTextCore(statusText); }));
            }
            else
            {
                this.SetToolStripStatusText(statusText);
            }
        }

        private void SetToolStripStatusTextCore(string statusText)
        {
            toolStripStatusLabel.Text = statusText;
            toolStripStatusLabel.Invalidate();
            statusStrip.Refresh();
        }

        private void StartPopulateListView()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { this.StartPopulateListViewCore(); }));
            }
            else 
            {
                this.StartPopulateListViewCore();
            }
        }

        private void StartPopulateListViewCore()
        {
            this.UseWaitCursor = true;

            this.RefreshGui(false);

            contactListViewItems.Clear();
            contactListView.Items.Clear();
        }

        private void StopPopulateListView()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate() { this.StopPopulateListViewCore(); }));
            }
            else
            {
                this.StopPopulateListViewCore();
            }
        }

        private void StopPopulateListViewCore()
        {
            this.UseWaitCursor = false;

            foreach (ContactListViewItem contact in contactListViewItems)
            {
                contactListView.Items.Add(contact.ToListViewItem());
            }

            this.RefreshGui(true);
        }

        #endregion

        private void PopulateContactListView()
        {
            try
            {
                this.StartPopulateListView();

                //Get all Active Directory contacts matching the filter
                this.SetToolStripStatusText(StringsRes.Lang.ReadingADContacts);
                ADContact[] adContacts = ADInterface.GetAdContacts(Properties.Settings.Default.CompanyName);

                //Get all Outlook contacts
                Outlook.Items olContacts = selectedOutlookContactsFolder.Items;

                contactListViewItems = ContactsHelper.FindNewAndUpdatedAdContacts(adContacts, olContacts);

                this.SetToolStripStatusText(StringsRes.Lang.ReadingOutlookContacts);
                ContactsHelper.FindOutlookContactsDeletedInAD(olContacts, Properties.Settings.Default.CompanyName, contactListViewItems);
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
                MessageBox.Show("Failed to check Outlook contacts. Error message: " + e.Message, StringsRes.Lang.MessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                this.SetToolStripStatusText(StringsRes.Lang.Done);
                this.StopPopulateListView();
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            this.RefreshListView();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            if (populateContactListViewThread != null)
            {
                shutdown = true;
                populateContactListViewThread.Join(1000);
            }
        }

        private void contactListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == listViewColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (listViewColumnSorter.SortOrder == SortOrder.Ascending)
                {
                    listViewColumnSorter.SortOrder = SortOrder.Descending;
                }
                else
                {
                    listViewColumnSorter.SortOrder = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                listViewColumnSorter.SortColumn = e.Column;
                listViewColumnSorter.SortOrder = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.contactListView.Sort();			
        }

        private void contactListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            ContactAction action = (ContactAction)contactListView.Items[e.Index].Tag;
            if (action == ContactAction.None)
            {
                e.NewValue = e.CurrentValue;
            }
        }
    }
}
