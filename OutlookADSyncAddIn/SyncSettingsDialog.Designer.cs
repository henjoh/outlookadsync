﻿namespace OutlookADAddIn
{
    partial class SyncSettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeButton = new System.Windows.Forms.Button();
            this.checkForChangesCheckBox = new System.Windows.Forms.CheckBox();
            this.enableLoggingCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contactFoldersComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(293, 145);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // checkForChangesCheckBox
            // 
            this.checkForChangesCheckBox.AutoSize = true;
            this.checkForChangesCheckBox.Location = new System.Drawing.Point(12, 92);
            this.checkForChangesCheckBox.Name = "checkForChangesCheckBox";
            this.checkForChangesCheckBox.Size = new System.Drawing.Size(198, 17);
            this.checkForChangesCheckBox.TabIndex = 1;
            this.checkForChangesCheckBox.Text = "Look for contact changes at start-up";
            this.checkForChangesCheckBox.UseVisualStyleBackColor = true;
            this.checkForChangesCheckBox.CheckedChanged += new System.EventHandler(this.checkForChangesCheckBox_CheckedChanged);
            // 
            // enableLoggingCheckBox
            // 
            this.enableLoggingCheckBox.AutoSize = true;
            this.enableLoggingCheckBox.Location = new System.Drawing.Point(12, 115);
            this.enableLoggingCheckBox.Name = "enableLoggingCheckBox";
            this.enableLoggingCheckBox.Size = new System.Drawing.Size(292, 17);
            this.enableLoggingCheckBox.TabIndex = 2;
            this.enableLoggingCheckBox.Text = "Log errors to file (\'[My Documents]\\OutlookADAddin.log\')";
            this.enableLoggingCheckBox.UseVisualStyleBackColor = true;
            this.enableLoggingCheckBox.CheckedChanged += new System.EventHandler(this.enableLoggingCheckBox_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Local contacts folder:";
            // 
            // contactFoldersComboBox
            // 
            this.contactFoldersComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contactFoldersComboBox.DisplayMember = "FullFolderPath";
            this.contactFoldersComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.contactFoldersComboBox.FormattingEnabled = true;
            this.contactFoldersComboBox.Location = new System.Drawing.Point(12, 65);
            this.contactFoldersComboBox.Name = "contactFoldersComboBox";
            this.contactFoldersComboBox.Size = new System.Drawing.Size(356, 21);
            this.contactFoldersComboBox.TabIndex = 7;
            this.contactFoldersComboBox.SelectionChangeCommitted += new System.EventHandler(this.contactFoldersComboBox_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Company Name";
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.companyNameTextBox.Location = new System.Drawing.Point(12, 26);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.Size = new System.Drawing.Size(356, 20);
            this.companyNameTextBox.TabIndex = 8;
            this.companyNameTextBox.Leave += new System.EventHandler(this.companyNameTextBox_Leave);
            // 
            // SyncSettingsDialog
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(380, 180);
            this.ControlBox = false;
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.contactFoldersComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enableLoggingCheckBox);
            this.Controls.Add(this.checkForChangesCheckBox);
            this.Controls.Add(this.closeButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SyncSettingsDialog";
            this.ShowInTaskbar = false;
            this.Text = "Synchronization Settings";
            this.Load += new System.EventHandler(this.SyncSettingsDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.CheckBox checkForChangesCheckBox;
        private System.Windows.Forms.CheckBox enableLoggingCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox contactFoldersComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox companyNameTextBox;
    }
}