﻿using System;
using System.IO;

namespace OutlookADAddIn.Helpers
{
    //TODO: Replace this with e.g. log4net
    class Logger
    {
        public static void Initialize()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.LogFile))
            {
                Properties.Settings.Default.LogFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "OutlookADAddin.log");
                //TODO: Save settings!?
                //TODO: Add option Truncate at start
                File.Create(Properties.Settings.Default.LogFile).Close();
            }

            Initialized = true;
        }

        public static bool Initialized { get; set; }

        public static void WriteLine(string message)
        {
            if (Initialized)
            {
                File.AppendAllText(Properties.Settings.Default.LogFile, string.Format("{0}  {1}", DateTime.Now.ToString(), message) + Environment.NewLine);
                System.Diagnostics.Trace.WriteLine(message);
            }
        }

        public static void WriteException(Exception exception)
        {
            if (Initialized)
            {
                File.AppendAllText(Properties.Settings.Default.LogFile, string.Format("{0}  Exception: {1}\n{2}", DateTime.Now.ToString(), exception.Message, exception.ToString()) + Environment.NewLine);
                System.Diagnostics.Trace.WriteLine(exception.ToString());
            }
        }
    }
}
