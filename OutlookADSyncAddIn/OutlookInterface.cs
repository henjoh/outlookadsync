﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookADAddIn
{
    internal class OutlookInterface
    {
        public static Outlook.MAPIFolder GetDefaultContactsFolder()
        {
            return Globals.OutlookADAddIn.Application.GetNamespace("MAPI").GetDefaultFolder(Outlook.OlDefaultFolders.olFolderContacts);
        }

        public static Outlook.MAPIFolder GetSelectedContactsFolder()
        {
            Outlook.MAPIFolder defaultOutlookContactsFolder = GetDefaultContactsFolder();
            Outlook.MAPIFolder selectedOutlookContactsFolder = defaultOutlookContactsFolder;
            foreach (Outlook.MAPIFolder f in defaultOutlookContactsFolder.Folders)
            {
                if (f.Name == Properties.Settings.Default.OutlookContactsFolder)
                {
                    selectedOutlookContactsFolder = f;
                    break;
                }
            }
            return selectedOutlookContactsFolder;
        }

        public static Outlook.ContactItem GetOutlookContact(Outlook.Items olContacts, string company, string name)
        {
            string filter = string.Format("[FullName] = '{0}' And [CompanyName] = '{1}'", name, company);
            return olContacts.Find(filter) as Outlook.ContactItem;
        }
    }
}
